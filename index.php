<?php require_once('inc/conf.php'); ?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= SITE; ?></title>
    <meta name="description" content="Web page template 1 design">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="img/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="img/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="img/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="img/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="img/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="img/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="img/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="img/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="img/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="<?= SITE; ?>"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="img/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="img/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="img/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="img/favicon/mstile-310x310.png" />

    <!-- stylesheets -->
    <link rel="stylesheet" href="css/style.min.css">

    <?php
    if (strlen(LIVERELOAD) > 1) :
        ?>
        <!-- livereload -->
        <script src="//<?= LIVERELOAD; ?>:35729/livereload.js"></script>
    <?php endif; ?>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="page-wrapper">

    <!-- page header -->
    <header id="home" class="page-header-wrapper">
        <div id="nav-section" class="header-content-top-wrapper cf">
            <div class="header-content-top left">
                <h1 class="logo">
                    <a id="home-logo" href="#home" class="logo-writting"><?= SITE; ?></a>
                    <a id="home-mobile" href="#home" class="fa fa-home"></a>
                </h1>
            </div>
            <div class="header-content-top right">
                <!-- page menu -->
                <nav class="header-main-nav">
                    <span id="nav-btn" class="fa fa-bars"></span>
                    <ul id="nav-list" class="nav-lm">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="header-content-inner">
            <div class="portfolio-logo">
                <img alt="Profile image" src="img/portfolio-logo-sample3.jpg">
            </div>
            <div class="portfolio-sign">
                <h2>Foo Bar</h2>
                <p>Web Developer | RHCE</p>
            </div>
            <div class="get-started">
                <a id="to-section1" class="fa fa-chevron-down" href="#section1"></a>
            </div>
        </div>
    </header>

    <!-- page body -->
    <main class="page-main-content">
        <div class="main-content-section copy-ready-std content-section-first">
            <section id="section1" class="content-wrapper about-it cf">
                <h1>WHAT WE DO</h1>
                <!-- left section -->
                <div class="about-it-content copy-ready-std left">
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-css3"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>CSS3</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-wordpress"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>WordPress</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-linux"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>LINUX</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
                <!-- right section -->
                <div class="about-it-content copy-ready-std right">
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-html5"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>HTML5</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-joomla"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>Joomla</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    <div class="about-it-inner-wrapper">
                        <div class="content-inner icon-box">
                            <span class="fa fa-drupal"></span>
                        </div>
                        <div class="content-inner content-box">
                            <h2>Drupal</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div id="about" class="main-content-section copy-ready-std content-section-second">
            <section class="content-wrapper about-it-more cf">
                <h1>WHO WE ARE</h1>
                <div class="about-it-content about-it-more-content left">
                    <span class="fa fa-briefcase"></span>
                </div>
                <div class="about-it-content about-it-more-content right">
                    <ul class="about-it-more-inner">
                        <li><span class="fa fa-check"></span><strong>PROFESIONALNOST</strong></li>
                        <li><span class="fa fa-check"></span><strong>STROKOVNOST</strong></li>
                        <li><span class="fa fa-check"></span><strong>ZANESLJIVOST</strong></li>
                    </ul>
                </div>
                <footer>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                </footer>
            </section>
        </div>
        <div id="contact" class="main-content-section content-section-third">
            <div class="main-content-section overlay-white">
                <section class="content-wrapper contact-box-wrapper cf">
                    <h1>CONTACT</h1>
                    <div class="about-it-content copy-ready-std contact-data-box vcard left">
                        <p>Polno Ime Podjetja d.o.o.</p>
                        <p>Prešernov Trg 1</p>
                        <p>1000 Ljubljana</p>
                        <p>Slovenija</p>
                    </div>
                    <div class="about-it-content copy-ready-std contact-form right">
                        <span id="form-toggle" class="fa fa-envelope"></span>
                        <form id="form" class="form-box disabled">
                            <label for="user-email-input">Email</label>
                            <input id="user-email-input" class="form-elements" type="text" name="user-email" placeholder="npr.: example@gmail.com">
                            <label for="user-fullname-input">Name</label>
                            <input id="user-fullname-input" class="form-elements" type="text" name="user-fullname" placeholder="npr.: Janez Novak">
                            <label for="user-subject-input">Subject</label>
                            <select id="user-subject-input" class="form-elements">
                                <option value="Izberi">Select</option>
                                <option value="Tema1">Tema1</option>
                            </select>
                            <label for="user-message-input">Message</label>
                            <textarea id="user-message-input" class="form-elements" placeholder="Vsebina sporočila.."></textarea>
                            <div id="loader" class="loader-box form-elements disabled">
                                <i class="fa fa-spinner fa-spin"></i>
                            </div>
                            <button id="form-submit" type="button">Submit</button>
                        </form>
                        <div id="response-positive" class="alert green disabled">
                            <p><strong>Your message was sent successfully. Thanks.</strong></p>
                        </div>
                        <div id="response-negative" class="alert red disabled">
                            <p><strong>Failed to send your message. Please try later or contact the administrator by another method.</strong></p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- section for google maps -->
        <div id="map-canvas" class="maps-box"></div>
    </main>

    <!-- page footer -->
    <footer class="page-footer-wrapper cf">
        <div class="left"></div>
        <div class="right">
            <?php if (SOCIAL) : ?>
                <div class="social-box-wrapper content-wrapper">
                    <a href="#" class="fa fa-twitter-square" title="Twitter"></a>
                    <a href="#" class="fa fa-dribbble" title="Dribbble"></a>
                    <a href="#" class="fa fa-instagram" title="Instagram"></a>
                    <a href="#" class="fa fa-facebook-square" title="Facebook"></a>
                    <a href="#" class="fa fa-google-plus-square" title="Google+"></a>
                </div>
            <?php endif; ?>
        </div>
        <div class="page-author-sign copy-ready-std">
            <?php date_default_timezone_set('Europe/Ljubljana'); ?>
            <small><a href="#home"><?= SITE; ?> &#169; <?= date('Y', time()); ?></a></small>
        </div>
    </footer>
</div>

<!-- scripts -->
<?php if (PRODUCTION) : ?>
    <?php if (GACODE) : ?>
    <?php
    // Create clientID
    function getClientId() {
        return md5(getenv("REMOTE_ADDR").'webdev'.getenv('HTTP_USER_AGENT'));
    }
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= GACODE; ?>', {'storage': 'none', 'clientId': '<?= getClientId(); ?>'});
        ga('send', 'pageview', {'anonymizeIp': true});
    </script>
    <?php endif; ?>
    <script src="js/script.min.js"></script>
<?php else : ?>
    <script src="js/built/script.js"></script>
<?php endif; ?>
</body>
</html>