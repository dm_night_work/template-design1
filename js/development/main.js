/**!
 * Main JS file for template file.
 * Author: deniss@gmail.com
 * Created by flye on 6/20/15.
 */
(function(document, window, Velocity) {

    /**
     * Variables - Init
     */

    var menu,
        menuList,
        toSection1,
        formBtn,
        submitBtn,
        xhr;

    menu       = document.getElementById('nav-btn');
    menuList   = document.getElementById('nav-list');
    toSection1 = document.getElementById('to-section1');
    formBtn    = document.getElementById('form-toggle');
    submitBtn  = document.getElementById('form-submit');

    if (window.XMLHttpRequest) { xhr = new XMLHttpRequest(); }
    else { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }

    /**
     * Event Listeners
     */

    addEvent(menu, 'click', function() { toggleMenu(menuList, ['active', 'animation-navigation-scrolldown']); });
    addEvent(toSection1, 'click', function(evt) { slideToContainer(evt); });

    addEvent(menuList, 'click', function(evt) {
        slideToContainer(evt);
        toggleMenu(menuList, ['active', 'animation-navigation-scrolldown']);
    });

    addEvent(formBtn, 'click', function() {
        var form;

        form = document.getElementById('form');

        this.classList.add('disabled');
        form.classList.remove('disabled');

        window.ga('send', 'event', 'button', 'click', 'form open button');
    });

    addEvent(submitBtn, 'click', function() {
        var email,
            name,
            msg,
            data,
            loader,
            responseMsg,
            alertError;

        email = document.getElementById('user-email-input');
        name  = document.getElementById('user-fullname-input');
        msg   = document.getElementById('user-message-input');

        if (validateEmail(email.value)) {
            if (email.classList.contains('field-alert')) { email.classList.remove('field-alert'); }

            data = {
                email: email.value,
                name: name.value,
                subject: document.getElementById('user-subject-input').value,
                msg: msg.value
            };
            loader      = document.getElementById('loader');
            responseMsg = document.getElementById('response-positive');
            alertError  = document.getElementById('response-negative');

            loader.classList.toggle('disabled');
            submitBtn.classList.toggle('disabled');

            if (data.name.length > 0 && data.msg.length > 0) {
                if (name.classList.contains('field-alert')) { name.classList.remove('field-alert'); }
                if (msg.classList.contains('field-alert')) { msg.classList.remove('field-alert'); }

                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        if (parseInt(xhr.responseText) == 1) {
                            responseMsg.classList.remove('disabled');
                            if (!alertError.classList.contains(('disabled'))) { alertError.classList.add('disabled'); }
                            loader.classList.toggle('disabled');

                            window.ga('send', 'event', 'button', 'click', 'form submit success');
                        } else {
                            alertError.classList.remove('disabled');
                            if (!responseMsg.classList.contains('disabled')) { responseMsg.classList.add('disabled'); }

                            window.ga('send', 'event', 'button', 'click', 'form submit error');
                        }
                    }
                };
                xhr.open('POST', 'inc/mailer.php', true);
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xhr.send("data="+encodeURIComponent(JSON.stringify(data)));
            } else {
                name.classList.add('field-alert');
                msg.classList.add('field-alert');

                loader.classList.toggle('disabled');
                submitBtn.classList.toggle('disabled');
            }
        } else {
            email.classList.add('field-alert');

            window.ga('send', 'event', 'button', 'click', 'form email error-validation');
        }

        window.ga('send', 'event', 'button', 'click', 'form submit button clicked');
    });

    /**
     * Functions
     */

    function toggleMenu(elem, activeClass) {
        var index,
            classLen;

        index = 0;
        classLen = activeClass.length;

        for (; index < classLen; ++index) {
            elem.classList.toggle(activeClass[index]);
        }
    }

    function slideToContainer(evt) {
        var target;

        target = evt.target.getAttribute('href').substring(1);

        Velocity(document.getElementById(target), 'scroll', { duration: 1200, offset: -82, easing: 'ease-in-out' });

        window.ga('send', 'event', 'button', 'click', 'scroll to-' + target);
    }

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    /* Helpers */
    function addEvent(elem, event, method) {
        if (elem.addEventListener) { elem.addEventListener(event, method, false); }
        else if (elem.attachEvent) { elem.attachEvent('on' + event, method); }
    }

    /**
     * Google Maps
     */

    window.initialize = function () {
        var mapLatLng,
            mapOptions,
            map,
            marker;

        mapLatLng  = new google.maps.LatLng(46.051380, 14.506270);
        mapOptions = {
            zoom: 15,
            center: mapLatLng,
            scrollwheel: false,
            draggable: false,
            zoomControle: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE
            }

        };
        map        = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        marker     = new google.maps.Marker({
            position: mapLatLng,
            map: map
        });
    };

    window.loadScript = function () {
        var script;

        script      = document.createElement('script');
        script.type = 'text/javascript';
        script.src  = 'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initialize';

        document.body.appendChild(script);
    };

    window.loadScript();

})(document, window, Velocity);