<?php
/**
 * Created by PhpStorm.
 * User: flye
 * Date: 6/23/15
 * Time: 7:06 PM
 */
require( 'conf.php' );

// get request from ajax
if (isset($_POST['data'])) { $data = json_decode($_POST['data'], true); }
else { die('Error!'); }

/**
 * Set the address of receiver
 */
$to = EMAIL;
$from = FROM;

// prepare data for email
$name    = $data['name'];
$subject = $data['subject'];
$msg     = $data['msg'];
$email    = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

// headers
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: <$from>" . "\r\n";

// message
$message  = '<body>';
$message .= 'Živjo!<br><br>';
$message .= 'Obiskovalec/ka <strong>' . $name . '</strong>, te želi nekaj vprašati oziroma ti nekaj povedati.<br>';
$message .= 'V spodnjih vrsticah je njegovo/njeno sporočilo:<br>';
$message .= '<p style="color: #754c78"><blockquote>"' . $msg .'"</blockquote></p>';
$message .= '<p style="color: #754c78">' . SITE . '</p>';
$message .= '</body>';

if (filter_var($from, FILTER_VALIDATE_EMAIL)) {
    if ( mail($to, $subject, $message, $headers) ) { echo true; }
    else { echo false; }
}
else { return false; }