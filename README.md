#Template file for single pages

**Some info about developing:**

1. clone the project in your projects dir
2. create your own branch (git checkout -b "name-of-your-branch")
3. always create pull request when code is complete to be merged into master
4. install grunt and all its dependencies (sudo npm install -> execute this command from root dir of project)
5. create conf.php file in INC directory (setup file is written in lower section)
6. when you are ready and feeling good for staring to develop, run from terminal "grunt live"

**SETTING UP CONF.PHP :**

1. create conf.php file in inc dir
2. define constants that are essentials for proper working project

#### My **conf.php** looks like that:
```php
<?php
// Essentials && Layout options
/* Set PRODUCTION to TRUE when you deploy project to production */
/* This option use production assets and aactivate GA tracking */
define('PRODUCTION', false);

/* Google Analytics tracking code */
define('GACODE', '-- insert your GA tracking code here --');

/* Set this to FALSE if you don't want to have social icons at all */
/* For social icons selection you need to modify index.php file */
define('SOCIAL', true);

/* Livereload IP's */
define('LIVERELOAD', '-- enter your IP --');

/* Your SITE name */
define('SITE', '-- enter site name --');

/* Email address */
define('EMAIL', '-- insert email address where you want emails to be sent --');
```

*.. more to be written*