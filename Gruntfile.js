module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bower_concat: {
            all: {
                dest: 'js/vendor/all.js',
                cssDest: 'css/vendor/all.css',
                exclude: [
                    'jquery'
                ],
                dependancies: {},
                bowerOptions: {
                    relative: false
                }
            }
        },

        concat_css: {
            all: {
                src: ['css/vendor/*.css', 'css/development/microclearfix.css', 'css/development/style.css'],
                dest: 'css/built/style.css'
            }
        },

        postcss: {
            options: {
                map: {
                    inline: false
                },

                processors: [
                    require('pixrem')(),
                    require('autoprefixer')({browser: 'last 2 versions'}),
                    require('cssnano')()
                ]
            },
            dist: {
                src: 'css/built/*.css',
                dest: 'css/style.min.css'
            }
        },


        concat: {
            dist: {
                src: [
                    'js/vendor/*.js',
                    'js/development/*.js'
                ],
                dest: 'js/built/script.js'
            }
        },

        uglify: {
            build: {
                src: 'js/built/*.js',
                dest: 'js/script.min.js'
            }
        },

        imagemin: {
            static: {
                options: {
                    optimizationLevel: 4
                },
                files: {
                    'img/*.jpg': 'img/*.jpg',
                    'img/*.png': 'img/*.png'
                }
            }
        },

        watch: {
            stylesheets: {
                files: ['css/development/*.css'],
                tasks: ['bower_concat', 'concat_css', 'postcss'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },

            scripts: {
                files: ['js/*.js'],
                tasks: ['bower_concat', 'concat', 'uglify'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },

            html: {
                files: ['**/*.html', '**/*.php'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },

            images: {
                files: ['img/*.jpg', 'img/*.png'],
                tasks: ['imagemin'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // Handling dependencies
    grunt.loadNpmTasks('grunt-bower-concat');

    // Handling custom scripts
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('load', ['bower_concat']);
    grunt.registerTask('css', ['concat_css', 'postcss']);
    grunt.registerTask('js', ['concat', 'uglify']);
    grunt.registerTask('live', ['css', 'js', 'watch']);
};